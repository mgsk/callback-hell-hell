;;;; package.lisp
;;;;
;;;; Copyright (c) 2015 Robert Smith <quad@symbo1ics.com>

(defpackage #:callback-hell
  (:use #:cl)
  (:export
   #:api-group
   #:api-group-function
   #:define-api-group
   #:api-function
   #:define-api-function
   #:compute-c-space-translation
   #:c-space-translation-function-index
   #:emit-includes
   #:emit-api-function-prototype
   #:emit-api-function-header
   #:emit-api-function-definitions
   #:emit-function-index-definition
   #:emit-h-file-contents
   #:emit-c-file-contents
   #:emit-library-files))

